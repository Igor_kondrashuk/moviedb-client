package com.example.moviedb.util.crypt

abstract class Crypto {
    protected val androidKeyStore = "AndroidKeyStore"
    protected val keyAlias = "TOKEN_PREFERENCES"

    abstract fun encrypt(byteArray: ByteArray): ByteArray

    abstract fun decrypt(byteArray: ByteArray): ByteArray
}