package com.example.moviedb.util

import io.reactivex.Completable
import io.reactivex.Maybe

abstract class BaseSharedPreferenceValue<T> {
    abstract fun saveObject(src: T): Completable

    abstract fun getObject(): Maybe<T>
}