package com.example.moviedb.util.crypt

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.security.KeyPairGeneratorSpec
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import java.math.BigInteger
import java.security.KeyPairGenerator
import java.security.KeyStore
import java.security.interfaces.RSAPrivateKey
import java.security.interfaces.RSAPublicKey
import java.util.*
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.spec.GCMParameterSpec
import javax.security.auth.x500.X500Principal

@TargetApi(Build.VERSION_CODES.M)
class CryptApi19(val context: Context): Crypto() {

    private val RSA_MODE = "RSA/ECB/PKCS1Padding"
    private val PROVIDER = "AndroidOpenSSL"

    init {
        val keyStore = KeyStore.getInstance(androidKeyStore)
        keyStore.load(null)

        if (!keyStore.containsAlias(keyAlias)) {
            val start = Calendar.getInstance()
            val end = Calendar.getInstance()
            end.add(Calendar.YEAR, 30)

            val spec = KeyPairGeneratorSpec.Builder(context)
                .setAlias(keyAlias)
                .setSubject(X500Principal("CN=$keyAlias"))
                .setSerialNumber(BigInteger.TEN)
                .setStartDate(start.time)
                .setEndDate(end.time)
                .build()
            val kpg = KeyPairGenerator.getInstance("RSA", androidKeyStore)
            kpg.initialize(spec)
            kpg.generateKeyPair()
        }
    }

    override fun encrypt(byteArray: ByteArray): ByteArray {
        val keyStore = KeyStore.getInstance(androidKeyStore)
        keyStore.load(null)
        val privateKeyEntry = keyStore.getEntry(keyAlias, null) as KeyStore.PrivateKeyEntry
        val publicKey = privateKeyEntry.certificate.publicKey as RSAPublicKey
        val cipher = Cipher.getInstance(RSA_MODE, PROVIDER)
        cipher.init(Cipher.ENCRYPT_MODE, publicKey)

        return cipher.doFinal(byteArray)
    }

    override fun decrypt(byteArray: ByteArray): ByteArray {
        val keyStore = KeyStore.getInstance(androidKeyStore)
        keyStore.load(null)

        val privateKeyEntry = keyStore.getEntry(keyAlias, null) as KeyStore.PrivateKeyEntry
        val privateKey = privateKeyEntry.privateKey as RSAPrivateKey
        val cipher = Cipher.getInstance(RSA_MODE, PROVIDER)
        cipher.init(Cipher.DECRYPT_MODE, privateKey)

        return cipher.doFinal(byteArray)
    }
}