package com.example.moviedb.util

import androidx.lifecycle.MutableLiveData
import android.content.*
import androidx.appcompat.widget.SearchView
import com.example.moviedb.data.api.response.ErrorResponse
import com.google.gson.Gson
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.disposables.Disposables
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import retrofit2.HttpException
import java.net.ConnectException
import java.net.UnknownHostException
import java.util.concurrent.TimeUnit

fun <T> Observable<T>.retryWithLinearBackoff(times: Int, deltaTimeDelay: Int, unit: TimeUnit): Observable<T> {
    return retryWhen { errors ->
        errors.zipWith(
            Observable.range(1, times + 1),
            BiFunction<Throwable, Int, Int> { error, retryCount ->
                if (retryCount > times) {
                    throw error
                } else {
                    retryCount
                }
            })
            .flatMap { retryCount ->
                Observable.timer((deltaTimeDelay * retryCount).toLong(), unit)
            }
    }
}

fun <T> callWhenInternetIsAvailable(internetStateSubject: BehaviorSubject<Boolean>, work: Single<T>): Single<T> {
    return internetStateSubject
        .observeOn(Schedulers.io())
        .filter { it }
        .firstOrError()
        .flatMap { work }
        .retryWhen { errors ->
            errors.flatMap { error ->
                if (error !is UnknownHostException && error !is ConnectException) {
                    throw error
                } else {
                    Flowable.timer(5, TimeUnit.SECONDS)
                }
            }
        }
}

fun callWhenInternetIsAvailable(internetStateSubject: BehaviorSubject<Boolean>, work: Completable): Completable {
    return internetStateSubject
        .observeOn(Schedulers.io())
        .filter { it }
        .firstOrError()
        .flatMapCompletable { work }
        .retryWhen { errors ->
            errors.flatMap { error ->
                if (error !is UnknownHostException && error !is ConnectException) {
                    throw error
                } else {
                    Flowable.timer(5, TimeUnit.SECONDS)
                }
            }
        }
}

fun <T> Single<T>.retryEndlessWithDelay(deltaTimeDelay: Long = 5, unit: TimeUnit = TimeUnit.SECONDS): Single<T> {
    return retryWhen { errors ->
        errors.flatMap {
            Flowable.timer(deltaTimeDelay, unit)
        }
    }
}

fun <T> Single<T>.subscribeWithLiveData(liveData: MutableLiveData<Resource<T>>): Disposable {
    return doOnSubscribe { liveData.postValue(Resource.loading()) }
        .subscribe({ liveData.postValue(Resource.success(it)) },
            {
                liveData.postValue(Resource.error(it))
            })
}

fun Completable.subscribeWithLiveData(liveData: MutableLiveData<CompletableResource>): Disposable {
    return doOnSubscribe { liveData.postValue(CompletableResource.loading()) }
        .subscribe({ liveData.postValue(CompletableResource.success()) },
            {
                liveData.postValue(CompletableResource.error(it))
            })
}

fun <T> Single<T>.applySchedulers(): Single<T> {
    return subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}

fun Completable.applySchedulers(): Completable {
    return subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}

fun Context.observeBroadcasts(action: String): Observable<Intent> {
    return observeBroadcasts(IntentFilter(action))
}

fun Context.observeBroadcasts(intentFilter: IntentFilter): Observable<Intent> {
    val observable = Observable.create<Intent> { observer ->
        val receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                observer.onNext(intent)
            }
        }

        observer.setDisposable(Disposables.fromRunnable {
            unregisterReceiver(receiver)
        })

        registerReceiver(receiver, intentFilter)
    }

    return observable
        .subscribeOn(Schedulers.io())
        .observeOn(Schedulers.io())
}

fun SearchView.getSearchQuery(initialQuery: String?): Observable<String> {
    val querySubject = PublishSubject.create<String>()
    if (initialQuery != null) {
        querySubject.onNext(initialQuery)
    } else {
        querySubject.onNext("")
    }
    setQuery(initialQuery, false)
    setOnQueryTextListener(object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String): Boolean {
            return true
        }

        override fun onQueryTextChange(query: String): Boolean {
            querySubject.onNext(query)
            return true
        }
    })
    return querySubject
        .map { it.trim() }
        .debounce(300, TimeUnit.MILLISECONDS)
        .distinctUntilChanged()
}

fun SharedPreferences.getIntMaybe(key: String, defaultValue: Int): Maybe<Int> {
    return Maybe.create<Int> { emitter ->
        try {
            if (contains(key)) {
                emitter.onSuccess(getInt(key, defaultValue))
            } else {
                emitter.onComplete()
            }
        } catch (error: Throwable) {
            emitter.onError(error)
        }
    }
}

fun SharedPreferences.saveObject(key: String, src: Any): Completable {
    return Completable.create { emitter ->
        try {
            edit().putString(key, Gson().toJson(src)).apply()
            emitter.onComplete()
        } catch (error: Throwable) {
            if (!emitter.isDisposed) {
                emitter.onError(error)
            }
        }
    }
}

inline fun <reified T> SharedPreferences.getObject(key: String): Maybe<T> {
    return Maybe.create { emitter ->
        try {
            if (contains(key)) {
                if (!emitter.isDisposed) {
                    val data = getString(key, "")
                    if(!data.isNullOrEmpty()) {
                        emitter.onSuccess(Gson().fromJson(data, T::class.java))
                    } else {
                        emitter.onComplete()
                    }
                } else {
                    emitter.onComplete()
                }
            } else {
                emitter.onComplete()
            }
        } catch (error: Throwable) {
            if (!emitter.isDisposed) {
                emitter.onError(error)
            }
        }
    }
}

fun HttpException.getErrorResponce(): ErrorResponse? {
    return response().errorBody()?.let {
        return Gson().fromJson(it.string(), ErrorResponse::class.java)
    }
}