package com.example.moviedb.util

import io.reactivex.Maybe
import io.reactivex.Single

class CachedValue<T>(
    private val baseSharedPreferenceValue: BaseSharedPreferenceValue<T>
) {

    private var cachedValue: T? = null

    fun getCachedValue(): Maybe<T> {
        return if (cachedValue != null) {
            Maybe.just(cachedValue)
        } else {
            baseSharedPreferenceValue.getObject()
                .doOnSuccess {
                    cachedValue = it
                }
        }

    }

}