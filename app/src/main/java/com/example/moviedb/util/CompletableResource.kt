package com.example.moviedb.util

class CompletableResource private constructor(val status: Status, val error: Throwable?) {

    companion object {
        fun loading(): CompletableResource {
            return CompletableResource(Status.LOADING, null)
        }
        fun success(): CompletableResource {
            return CompletableResource(Status.SUCCESS, null)
        }
        fun error(throwable: Throwable): CompletableResource {
            return CompletableResource(Status.ERROR, throwable)
        }
    }

    enum class Status {
        SUCCESS,
        LOADING,
        ERROR
    }
}