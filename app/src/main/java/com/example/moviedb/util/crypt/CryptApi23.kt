package com.example.moviedb.util.crypt

import android.annotation.TargetApi
import android.os.Build
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import java.security.KeyStore
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.spec.GCMParameterSpec

@TargetApi(Build.VERSION_CODES.M)
class CryptApi23 : Crypto() {
    private val ivSize = 12

    override fun encrypt(byteArray: ByteArray): ByteArray {
        val keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, androidKeyStore)
        val keyGenParameterSpec =
            KeyGenParameterSpec.Builder(keyAlias, KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
                .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
                .build()
        keyGenerator.init(keyGenParameterSpec)
        val secretKey = keyGenerator.generateKey()

        val cipher = Cipher.getInstance("AES/GCM/NoPadding")
        cipher.init(Cipher.ENCRYPT_MODE, secretKey)

        return cipher.iv + cipher.doFinal(byteArray)
    }

    override fun decrypt(byteArray: ByteArray): ByteArray {
        val keyStore = KeyStore.getInstance(androidKeyStore)
        keyStore.load(null)
        val secretKeyEntry = keyStore.getEntry(keyAlias, null) as KeyStore.SecretKeyEntry
        val secretKey = secretKeyEntry.secretKey
        val cipher = Cipher.getInstance("AES/GCM/NoPadding")
        val spec = GCMParameterSpec(128, byteArray.copyOfRange(0, ivSize))
        cipher.init(Cipher.DECRYPT_MODE, secretKey, spec)
        return cipher.doFinal(byteArray.copyOfRange(ivSize, byteArray.size))
    }
}