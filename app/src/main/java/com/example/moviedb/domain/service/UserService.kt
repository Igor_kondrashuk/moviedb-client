package com.example.moviedb.domain.service

import com.example.moviedb.data.api.response.MovieAccountStatesResponse
import com.example.moviedb.domain.entity.AccountDetails
import io.reactivex.Completable
import io.reactivex.Single

interface UserService {
    fun createUserSession(login: String, password: String): Single<AccountDetails>
    fun getAccountDetails(): Single<AccountDetails>
    fun rateMovie(movieId: String, value: Float): Single<Float>
    fun cancelRatingMovie(movieId: String): Completable
    fun getMovieAccountStates(movieId: String): Single<MovieAccountStatesResponse>
}