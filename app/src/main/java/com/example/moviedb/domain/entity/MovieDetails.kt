package com.example.moviedb.domain.entity

import com.google.gson.annotations.SerializedName

data class MovieDetails(
    @SerializedName("id")
    val id: String,
    @SerializedName("adult")
    val adult: Boolean,
    @SerializedName("homepage")
    val homepage: String,
    @SerializedName("poster_path")
    val posterUrl: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("runtime")
    val runtime: String,
    @SerializedName("release_date")
    val releaseDate: String,
    @SerializedName("budget")
    val budget: Int,
    @SerializedName("revenue")
    val revenue: Int,
    @SerializedName("original_title")
    val originalTitle: String
)