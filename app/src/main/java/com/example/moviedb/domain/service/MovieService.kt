package com.example.moviedb.domain.service

import com.example.moviedb.data.repository.movie.MovieDataSource
import com.example.moviedb.data.repository.movie.MovieResult
import com.example.moviedb.domain.entity.Movie
import com.example.moviedb.domain.entity.MovieDetails
import io.reactivex.Single

interface MovieService {
    fun getMovieById(movieId: String): Single<MovieDetails>
    fun getMovies(searchQuery: String, queryType: MovieDataSource.QueryType): MovieResult
    fun getMovieDetails(movieId: String)
}