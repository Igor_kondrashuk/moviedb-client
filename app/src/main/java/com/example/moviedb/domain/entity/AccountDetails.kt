package com.example.moviedb.domain.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AccountDetails(
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String
    ) : Parcelable