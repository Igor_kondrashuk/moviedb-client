package com.example.moviedb.ui.login

import android.os.Bundle
import android.security.keystore.KeyInfo
import android.view.View
import androidx.lifecycle.Observer
import com.example.moviedb.R
import com.example.moviedb.data.api.response.StatusCode
import com.example.moviedb.ui.base.activity.BaseActivity
import com.example.moviedb.util.Resource
import com.example.moviedb.util.crypt.CryptApi19
import com.example.moviedb.util.crypt.CryptApi23
import com.example.moviedb.util.getErrorResponce
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.HttpException


class LoginActivity : BaseActivity() {

    private val loginViewModel: LoginViewModel by lazy {
        getViewModel<LoginViewModel>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        loginViewModel.loginIdLiveData.observe(this, Observer {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    finish()
                }
                Resource.Status.ERROR -> {
                    if (it.error is HttpException) {
                        it.error.getErrorResponce()?.let { errorResponse ->
                            setLoginError(
                                when (errorResponse.statusCode) {
                                    StatusCode.INVALID_USERNAME_AND_PASSWORD -> getString(R.string.wrong_login_or_password)
                                    StatusCode.ACCOUNT_IS_DISABLED -> getString(R.string.account_is_disabled)
                                    StatusCode.EMAIL_NOT_VERIFIED -> getString(R.string.email_not_verified)
                                    else -> {
                                        Snackbar.make(rootView, errorResponse.statusMessage, Snackbar.LENGTH_LONG)
                                            .show()
                                        null
                                    }
                                }
                            )
                        }
                    } else {
                        it.error?.message?.let { errorMessage ->
                            Snackbar.make(rootView, errorMessage, Snackbar.LENGTH_LONG)
                                .show()
                        }
                    }
                    progressBar.visibility = View.INVISIBLE
                    loginButton.isEnabled = true
                }
                Resource.Status.LOADING -> {
                    setLoginError(null)
                    progressBar.visibility = View.VISIBLE
                    loginButton.isEnabled = false
                }
            }
        })

        loginButton.setOnClickListener {
            if (loginEditText.text.isNullOrEmpty()) {
                loginTextInput.error = "You must provide a username"
            }
            if (passwordEditText.text.isNullOrEmpty()) {
                passwordTextInput.error = "You must provide a password"
            }
            if (!loginEditText.text.isNullOrEmpty() && !passwordEditText.text.isNullOrEmpty()) {
                loginViewModel.login(loginEditText.text.toString(), passwordEditText.text.toString())
            }
        }

        val errorSnackbar = Snackbar.make(
            rootView,
            getString(R.string.connection_error),
            Snackbar.LENGTH_INDEFINITE
        )

        loginViewModel.hasInternetLiveData.observe(this, Observer { hasInternet ->
            hasInternet?.let {
                if (!hasInternet && !errorSnackbar.isShown) {
                    errorSnackbar.show()
                } else if (hasInternet && errorSnackbar.isShown) {
                    errorSnackbar.dismiss()
                }
            }
        })
    }

    private fun setLoginError(errorMessage: String?) {
        loginTextInput.error = if (errorMessage != null) " " else errorMessage
        passwordTextInput.error = errorMessage
    }
}