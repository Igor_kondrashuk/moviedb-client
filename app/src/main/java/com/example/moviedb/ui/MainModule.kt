package com.example.moviedb.ui

import androidx.lifecycle.ViewModel
import com.example.moviedb.di.ViewModelKey
import com.example.moviedb.di.ViewModelModule
import com.example.moviedb.ui.movies.list.MoviesFragment
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module(includes = [ViewModelModule::class])
abstract class MainModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    internal abstract fun bindMainViewModel(viewModel: MainViewModel): ViewModel

    @ContributesAndroidInjector
    internal abstract fun moviesFragment(): MoviesFragment
}