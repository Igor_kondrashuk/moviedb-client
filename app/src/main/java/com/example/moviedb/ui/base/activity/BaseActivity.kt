package com.example.moviedb.ui.base.activity

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.content.IntentFilter
import com.example.moviedb.broadcastreceiver.NetworkChangedBroadcastReceiver
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

abstract class BaseActivity : DaggerAppCompatActivity() {

    protected val disposables = CompositeDisposable()

    @Inject
    lateinit var networkChangedBroadcastReceiver: NetworkChangedBroadcastReceiver

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    inline fun <reified T : ViewModel> getViewModel(): T {
        return ViewModelProviders.of(this, viewModelFactory)[T::class.java]
    }

    override fun onResume() {
        super.onResume()
        registerReceiver(networkChangedBroadcastReceiver, IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))
    }

    override fun onPause() {
        unregisterReceiver(networkChangedBroadcastReceiver)
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }
}