package com.example.moviedb.ui.movies.detail

import androidx.lifecycle.MutableLiveData
import com.example.moviedb.data.api.response.MovieAccountStatesResponse
import com.example.moviedb.data.api.response.RateMovie
import com.example.moviedb.data.repository.movie.MovieDataSource
import com.example.moviedb.data.repository.movie.MovieResult
import com.example.moviedb.domain.entity.AccountDetails
import com.example.moviedb.domain.entity.MovieDetails
import com.example.moviedb.domain.service.MovieService
import com.example.moviedb.domain.service.UserService
import com.example.moviedb.ui.base.BaseViewModel
import com.example.moviedb.util.CompletableResource
import com.example.moviedb.util.Resource
import com.example.moviedb.util.applySchedulers
import com.example.moviedb.util.subscribeWithLiveData
import io.reactivex.rxkotlin.addTo
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject


class MoviesDetailViewModel @Inject constructor(
    private val userService: UserService,
    internetStateSubject: BehaviorSubject<Boolean>,
    private val movieService: MovieService
) : BaseViewModel() {

    val hasInternetLiveData = MutableLiveData<Boolean>()

    val movieDetailsLiveData = MutableLiveData<Resource<MovieDetails>>()

    val accountDetailsLiveData = MutableLiveData<Resource<AccountDetails>>()

    val movieAccountStatesLiveData = MutableLiveData<Resource<MovieAccountStatesResponse>>()

    val progressLiveData = MutableLiveData<CompletableResource>()

    val rateMovieLiveData = MutableLiveData<RateMovie>()

    init {
        internetStateSubject.subscribe { hasInternet ->
            hasInternetLiveData.postValue(hasInternet)
        }.addTo(disposables)
    }

    fun getMovieDetails(movieId: String) {
        movieService.getMovieById(movieId)
            .applySchedulers()
            .subscribeWithLiveData(movieDetailsLiveData)
    }

    fun getMovieAccountStates(movieId: String) {
        userService.getMovieAccountStates(movieId)
            .doOnSuccess {
                rateMovieLiveData.postValue(it.rated)
            }
            .applySchedulers()
            .subscribeWithLiveData(movieAccountStatesLiveData)
    }

    fun rateMovie(movieId: String, value: Float) {
        userService.rateMovie(movieId, value)
            .applySchedulers()
            .doOnSubscribe { progressLiveData.postValue(CompletableResource.loading()) }
            .doOnSuccess {
                rateMovieLiveData.postValue(RateMovie(true, value))
            }
            .subscribe({ progressLiveData.postValue(CompletableResource.success()) },
                {
                    progressLiveData.postValue(CompletableResource.error(it))
                })
            .addTo(disposables)
    }

    fun cancelRateMovie(movieId: String) {
        userService.cancelRatingMovie(movieId)
            .applySchedulers()
            .doOnSubscribe { progressLiveData.postValue(CompletableResource.loading()) }
            .doOnComplete {
                rateMovieLiveData.postValue(RateMovie(false, 0f))
            }
            .subscribe({ progressLiveData.postValue(CompletableResource.success()) },
                {
                    progressLiveData.postValue(CompletableResource.error(it))
                })
            .addTo(disposables)
    }
}