package com.example.moviedb.ui.movies.detail

import androidx.lifecycle.ViewModel
import com.example.moviedb.di.ViewModelKey
import com.example.moviedb.di.ViewModelModule
import com.example.moviedb.ui.movies.list.MoviesFragment
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module(includes = [ViewModelModule::class])
abstract class MoviesDetailModule {

    @Binds
    @IntoMap
    @ViewModelKey(MoviesDetailViewModel::class)
    internal abstract fun bindMoviesDetailViewModel(viewModel: MoviesDetailViewModel): ViewModel
}