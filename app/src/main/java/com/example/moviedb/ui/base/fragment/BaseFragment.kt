package com.example.moviedb.ui.base.fragment


import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.support.DaggerFragment
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

open class BaseFragment : DaggerFragment() {

    protected val disposables = CompositeDisposable()

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    inline fun <reified T : ViewModel> getFragmentViewModel(): T {
        return ViewModelProviders.of(this, viewModelFactory)[T::class.java]
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }
}