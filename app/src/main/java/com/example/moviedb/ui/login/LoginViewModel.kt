package com.example.moviedb.ui.login

import androidx.lifecycle.MutableLiveData
import com.example.moviedb.domain.entity.AccountDetails
import com.example.moviedb.domain.service.UserService
import com.example.moviedb.ui.base.BaseViewModel
import com.example.moviedb.util.Resource
import com.example.moviedb.util.applySchedulers
import com.example.moviedb.util.subscribeWithLiveData
import io.reactivex.rxkotlin.addTo
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject


class LoginViewModel @Inject constructor(
    private val userService: UserService,
    internetStateSubject: BehaviorSubject<Boolean>
) : BaseViewModel() {

    val hasInternetLiveData = MutableLiveData<Boolean>()

    init {
        internetStateSubject.subscribe { hasInternet ->
            hasInternetLiveData.postValue(hasInternet)
        }.addTo(disposables)
    }
    val loginIdLiveData = MutableLiveData<Resource<AccountDetails>>()

    fun login(login: String, password: String) {
        userService.createUserSession(login, password)
            .applySchedulers()
            .subscribeWithLiveData(loginIdLiveData)
            .addTo(disposables)
    }
}