package com.example.moviedb.ui.base.fragment


import android.content.Context
import com.example.moviedb.ui.base.ToolbarCallbacks
import io.reactivex.Observable

abstract class BaseToolbarFragment : BaseFragment(), ToolbarCallbacks {
    private lateinit var toolbarCallbacks: ToolbarCallbacks

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            toolbarCallbacks = context as ToolbarCallbacks
        } catch (error: ClassCastException) {
            throw ClassCastException(context.toString() + "must implement ToolbarCallbacks")
        }
    }

    override fun setTitle(titleRes: Int) {
        toolbarCallbacks.setTitle(titleRes)
    }

    override fun setTitle(title: String) {
        toolbarCallbacks.setTitle(title)
    }

    override fun setSubtitle(subtitleRes: Int) {
        toolbarCallbacks.setSubtitle(subtitleRes)
    }

    override fun setSubtitle(subtitle: String) {
        toolbarCallbacks.setSubtitle(subtitle)
    }

    override fun setSearchView(menuId: Int, initialQuery: String): Observable<String> {
        return toolbarCallbacks.setSearchView(menuId, initialQuery)
    }

    override fun clearSearchQuery() {
        toolbarCallbacks.clearSearchQuery()
    }

}