package com.example.moviedb.ui.base

import androidx.annotation.StringRes
import io.reactivex.Observable


interface ToolbarCallbacks {

    fun setTitle(@StringRes titleRes: Int)

    fun setTitle(title: String)

    fun setSubtitle(@StringRes subtitleRes: Int)

    fun setSubtitle(subtitle: String)

    fun setSearchView(menuId: Int, initialQuery: String): Observable<String>

    fun clearSearchQuery()

}