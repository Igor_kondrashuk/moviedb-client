package com.example.moviedb.ui.movies.list

import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.moviedb.BuildConfig
import com.example.moviedb.R
import com.example.moviedb.domain.entity.Movie
import kotlinx.android.synthetic.main.item_movie.view.*

class MoviesPagedAdapter : PagedListAdapter<Movie, androidx.recyclerview.widget.RecyclerView.ViewHolder>(
    DIFF_CALLBACK
) {
    var onItemClick: ((Movie) -> Unit)? = null

    var loadingState: LoadingState =
        LoadingState.loading()
        set(value) {
            notifyItemChanged(itemCount)
            field = value
        }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder =
        when (viewType) {
            TYPE_LOADING -> LoadingViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_loading,
                    parent,
                    false
                )
            )
            else -> MovieViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_movie, parent, false
                )
            )
        }


    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is MovieViewHolder -> holder.init(position)
            is LoadingViewHolder -> holder.init()
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if ((loadingState.status == LoadingState.Status.LOADING ||
                    loadingState.status == LoadingState.Status.ERROR) && position == itemCount - 1
        ) {
            TYPE_LOADING
        } else {
            TYPE_MOVIE
        }
    }

    inner class MovieViewHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        fun init(position: Int) {
            getItem(position)?.let { movie ->
                Glide
                    .with(itemView.context)
                    .load(BuildConfig.IMAGE_BASEURL + movie.posterUrl)
                    .thumbnail()
                    .placeholder(R.drawable.ic_movie_bounded)
                    .centerCrop()
                    .into(itemView.movieImage)
                itemView.name.text = movie.title
                itemView.setOnClickListener {
                    onItemClick?.invoke(movie)
                }
            }
        }
    }

    inner class LoadingViewHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        fun init() {
        }
    }

    companion object {
        private const val TYPE_MOVIE = 0
        private const val TYPE_LOADING = 1

        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Movie>() {
            override fun areItemsTheSame(movieOld: Movie, movieNew: Movie): Boolean {
                return movieNew.id == movieNew.id
            }

            override fun areContentsTheSame(movieOld: Movie, movieNew: Movie): Boolean {
                return movieNew == movieOld
            }
        }
    }
}