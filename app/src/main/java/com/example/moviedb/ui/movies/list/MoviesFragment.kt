package com.example.moviedb.ui.movies.list

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.example.moviedb.R
import com.example.moviedb.data.repository.movie.MovieDataSource
import com.example.moviedb.ui.base.fragment.BaseToolbarFragment
import com.example.moviedb.ui.movies.detail.MovieDetailActivity
import com.google.android.material.snackbar.Snackbar
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.fragment_movies.*


class MoviesFragment : BaseToolbarFragment() {

    private val moviesViewModel: MoviesViewModel by lazy {
        getFragmentViewModel<MoviesViewModel>()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_movies, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        moviesRecyclerView.layoutManager = GridLayoutManager(context, 3)
        moviesRecyclerView.setHasFixedSize(true)

        val adapter = MoviesPagedAdapter()

        moviesViewModel.movieResultLiveData.observe(this, Observer {
            it?.let { movieResult ->
                adapter.submitList(null)
                movieResult.data.observe(this, Observer { pagedList ->
                    adapter.submitList(pagedList)
                })
                movieResult.loadingState.observe(this, Observer { loadingState ->
                    loadingState?.let { networkState ->
                        adapter.loadingState = networkState
                        if (networkState.status == LoadingState.Status.INIT_LOADING) {
                            initialProgressBar.visibility = View.VISIBLE
                        } else {
                            initialProgressBar.visibility = View.INVISIBLE
                        }
                        if (networkState.status == LoadingState.Status.EMPTY) {
                            empty.visibility = View.VISIBLE
                        } else {
                            empty.visibility = View.INVISIBLE
                        }
                    }
                })
            }
        })

        val errorSnackbar = Snackbar.make(
            placeSnackBar,
            getString(R.string.connection_error),
            Snackbar.LENGTH_INDEFINITE
        )

        moviesViewModel.hasInternetLiveData.observe(this, Observer { hasInternet ->
            hasInternet?.let {
                if (!hasInternet && !errorSnackbar.isShown) {
                    errorSnackbar.show()
                } else if (hasInternet && errorSnackbar.isShown) {
                    errorSnackbar.dismiss()
                }
            }
        })

        moviesViewModel.queryTypeLiveData.observe(this, Observer { queryType ->
            queryType?.let {
                when (queryType) {
                    MovieDataSource.QueryType.POPULAR -> setTitle(R.string.popular)
                    MovieDataSource.QueryType.TOP_RATED -> setTitle(R.string.top_rated)
                    MovieDataSource.QueryType.UPCOMING -> setTitle(R.string.upcoming)
                }
            }
        })

        moviesRecyclerView.adapter = adapter

        adapter.onItemClick = { movie ->
            context?.let { context ->
                startActivity(
                    Intent(context, MovieDetailActivity::class.java)
                        .putExtra(MovieDetailActivity.MOVIE, movie)
                )
            }
        }

        bottomNavigation.setOnNavigationItemSelectedListener {
            if (it.isChecked) {
                return@setOnNavigationItemSelectedListener true
            }
            clearSearchQuery()
            when (it.itemId) {
                R.id.action_popular -> {
                    moviesViewModel.getMovies(MovieDataSource.QueryType.POPULAR)
                }
                R.id.action_top_rated -> {
                    moviesViewModel.getMovies(MovieDataSource.QueryType.TOP_RATED)
                }
                R.id.action_upcoming -> {
                    moviesViewModel.getMovies(MovieDataSource.QueryType.UPCOMING)
                }
            }
            return@setOnNavigationItemSelectedListener true
        }
        if (savedInstanceState == null) {
            moviesViewModel.getMovies(MovieDataSource.QueryType.POPULAR)
        }
        setSearchView(R.menu.search_menu, "")
            .subscribe { query ->
                moviesViewModel.setSearchFilter(query)
            }.addTo(disposables)
    }

}