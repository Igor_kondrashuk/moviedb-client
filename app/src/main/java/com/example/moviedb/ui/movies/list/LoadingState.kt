package com.example.moviedb.ui.movies.list

class LoadingState private constructor(val status: Status, val error: Throwable?) {

    companion object {
        fun loading(): LoadingState {
            return LoadingState(
                Status.LOADING,
                null
            )
        }

        fun initLoading(): LoadingState {
            return LoadingState(
                Status.INIT_LOADING,
                null
            )
        }

        fun success(): LoadingState {
            return LoadingState(
                Status.LOADED,
                null
            )
        }

        fun error(throwable: Throwable): LoadingState {
            return LoadingState(
                Status.ERROR,
                throwable
            )
        }

        fun empty(): LoadingState {
            return LoadingState(
                Status.EMPTY,
                null
            )
        }
    }

    enum class Status {
        LOADED,
        INIT_LOADING,
        LOADING,
        ERROR,
        EMPTY
    }
}