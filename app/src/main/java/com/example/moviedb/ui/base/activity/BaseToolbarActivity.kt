package com.example.moviedb.ui.base.activity

import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import com.example.moviedb.R
import com.example.moviedb.ui.base.ToolbarCallbacks
import com.example.moviedb.util.getSearchQuery
import io.reactivex.Observable

abstract class BaseToolbarActivity : BaseActivity(), ToolbarCallbacks {
    protected lateinit var activityToolbar: Toolbar

    override fun setTitle(titleRes: Int) {
        activityToolbar.setTitle(titleRes)
    }

    override fun setTitle(title: String) {
        activityToolbar.title = title
    }

    override fun setSubtitle(subtitleRes: Int) {
        activityToolbar.setSubtitle(subtitleRes)
    }

    override fun setSubtitle(subtitle: String) {
        activityToolbar.subtitle = subtitle
    }

    override fun setSearchView(menuId: Int, initialQuery: String): Observable<String> {
        activityToolbar.menu.clear()
        activityToolbar.inflateMenu(menuId)
        val searchItem = activityToolbar.menu.findItem(R.id.actionSearch)
        val searchView = searchItem.actionView as SearchView
        if (initialQuery.isNotEmpty()) {
            searchView.isIconified = false
            searchItem.expandActionView()
            searchView.clearFocus()
        }
        return searchView.getSearchQuery(initialQuery)
    }

    override fun clearSearchQuery() {
        val searchItem = activityToolbar.menu.findItem(R.id.actionSearch)
        searchItem.collapseActionView()
    }
}