package com.example.moviedb.ui

import android.os.Bundle
import android.view.MenuItem
import androidx.core.view.GravityCompat
import com.example.moviedb.R
import com.example.moviedb.R.id.action_movies_screen
import com.example.moviedb.ui.base.activity.BaseToolbarActivity
import com.example.moviedb.ui.movies.list.MoviesFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar.*

class MainActivity : BaseToolbarActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        activityToolbar = toolbar
        navigationDrawer.setNavigationItemSelectedListener {
            drawerLayout.closeDrawers()
            if (it.isChecked) {
                return@setNavigationItemSelectedListener true
            }
            when (it.itemId) {
                action_movies_screen -> changeFragment(MoviesFragment())
            }
            return@setNavigationItemSelectedListener true
        }
        if (savedInstanceState == null) {
            navigationDrawer.setCheckedItem(R.id.action_movies_screen)
            changeFragment(MoviesFragment())
        }
        activityToolbar.setNavigationOnClickListener {
            drawerLayout.openDrawer(GravityCompat.START)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                drawerLayout.openDrawer(GravityCompat.START)
                return true
            }
        }
        return true
    }

    private fun changeFragment(fragment: androidx.fragment.app.Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentContainer, fragment)
            .commit()
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }
}