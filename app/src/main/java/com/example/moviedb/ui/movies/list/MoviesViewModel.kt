package com.example.moviedb.ui.movies.list

import androidx.lifecycle.MutableLiveData
import com.example.moviedb.data.repository.movie.MovieDataSource
import com.example.moviedb.data.repository.movie.MovieResult
import com.example.moviedb.domain.service.MovieService
import com.example.moviedb.ui.base.BaseViewModel
import io.reactivex.rxkotlin.addTo
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject


class MoviesViewModel @Inject constructor(
    private val movieService: MovieService,
    internetStateSubject: BehaviorSubject<Boolean>
) : BaseViewModel() {

    val hasInternetLiveData = MutableLiveData<Boolean>()

    val movieResultLiveData = MutableLiveData<MovieResult>()

    private val searchQueryLiveData = MutableLiveData<String>()

    val queryTypeLiveData = MutableLiveData<MovieDataSource.QueryType>()

    init {
        internetStateSubject.subscribe { hasInternet ->
            hasInternetLiveData.postValue(hasInternet)
        }.addTo(disposables)
        searchQueryLiveData.observeForever {
            queryTypeLiveData.value?.let {
                getMovies(it)
            }
        }
    }

    fun getMovies(queryType: MovieDataSource.QueryType) {
        queryTypeLiveData.postValue(queryType)
        movieResultLiveData.postValue(movieService.getMovies(searchQueryLiveData.value ?: "", queryType))
    }

    fun setSearchFilter(searchQuery: String = "") {
        if (searchQueryLiveData.value != searchQuery) {
            searchQueryLiveData.postValue(searchQuery)
        }
    }
}