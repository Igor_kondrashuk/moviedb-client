package com.example.moviedb.ui.movies.list

import androidx.lifecycle.ViewModel
import com.example.moviedb.di.ViewModelKey
import com.example.moviedb.di.ViewModelModule
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module(includes = [ViewModelModule::class])
abstract class MoviesModule {

    @Binds
    @IntoMap
    @ViewModelKey(MoviesViewModel::class)
    internal abstract fun bindMoviesViewModel(viewModel: MoviesViewModel): ViewModel
}