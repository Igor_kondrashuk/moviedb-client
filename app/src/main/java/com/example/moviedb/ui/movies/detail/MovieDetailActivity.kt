package com.example.moviedb.ui.movies.detail

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.example.moviedb.BuildConfig
import com.example.moviedb.R
import com.example.moviedb.domain.entity.Movie
import com.example.moviedb.ui.base.activity.BaseActivity
import com.example.moviedb.ui.login.LoginActivity
import com.example.moviedb.util.AuthException
import com.example.moviedb.util.CompletableResource
import com.example.moviedb.util.Resource
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_movie_detail.*

class MovieDetailActivity : BaseActivity() {

    lateinit var movie: Movie

    companion object {
        const val MOVIE = "MOVIE"
    }

    private val moviesDetailViewModel: MoviesDetailViewModel by lazy {
        getViewModel<MoviesDetailViewModel>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail)
        setSupportActionBar(toolbar)
        supportActionBar?.setHomeButtonEnabled(true)
        movie = intent.getParcelableExtra(MOVIE)
        title = movie.title
        overview.text = movie.overview
        Glide.with(this)
            .load(BuildConfig.IMAGE_BASEURL + movie.posterUrl)
            .thumbnail()
            .placeholder(R.drawable.ic_movie_bounded)
            .centerCrop()
            .into(movieImage)

        val errorSnackbar = Snackbar.make(
            container,
            getString(R.string.connection_error),
            Snackbar.LENGTH_INDEFINITE
        )

        moviesDetailViewModel.hasInternetLiveData.observe(this, Observer { hasInternet ->
            hasInternet?.let {
                if (!hasInternet && !errorSnackbar.isShown) {
                    errorSnackbar.show()
                } else if (hasInternet && errorSnackbar.isShown) {
                    errorSnackbar.dismiss()
                }
            }
        })

        moviesDetailViewModel.movieDetailsLiveData.observe(this, Observer { resource ->
            when (resource.status) {
                Resource.Status.LOADING -> {
                    loadingDetails.visibility = View.VISIBLE
                    detailContainer.visibility = View.INVISIBLE
                }
                Resource.Status.SUCCESS -> {
                    loadingDetails.visibility = View.GONE
                    detailContainer.visibility = View.VISIBLE
                    resource.data?.let { movieDetails ->
                        originalTitle.text = movieDetails.originalTitle
                        status.text = movieDetails.status
                        runtime.text = movieDetails.runtime
                        premiere.text = movieDetails.releaseDate
                        budget.text = getString(R.string.price_dollar, movieDetails.budget)
                        revenue.text = getString(R.string.price_dollar, movieDetails.revenue)
                        homepage.text = movieDetails.homepage
                    }
                }
                Resource.Status.ERROR -> {
                    resource.error?.let {
                        Snackbar.make(
                            container,
                            it.toString(),
                            Snackbar.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        })
        moviesDetailViewModel.accountDetailsLiveData.observe(this, Observer { resource ->
            when (resource.status) {
                Resource.Status.SUCCESS -> {
                }
                Resource.Status.ERROR -> {
                    resource.error?.let { error ->
                        if (error is AuthException) {
                            Snackbar.make(container, getString(R.string.login_required), Snackbar.LENGTH_LONG)
                                .apply {
                                    setAction(R.string.login) {
                                        startActivity(Intent(this@MovieDetailActivity, LoginActivity::class.java))
                                    }
                                    setActionTextColor(ContextCompat.getColor(context, R.color.colorAccent))
                                }.show()
                        }
                    }
                }
            }
        })
        moviesDetailViewModel.progressLiveData.observe(this, Observer { resource ->
            when (resource.status) {
                CompletableResource.Status.LOADING -> {
                    progressBar.visibility = View.VISIBLE
                }
                CompletableResource.Status.SUCCESS -> {
                    progressBar.visibility = View.INVISIBLE
                }
                CompletableResource.Status.ERROR -> {
                    progressBar.visibility = View.INVISIBLE
                }
            }
        })

        cancelRate.setOnClickListener {
            moviesDetailViewModel.cancelRateMovie(movie.id)
        }

        ratingBarMovie.setOnRatingBarChangeListener { ratingBar, rating, fromUser ->
            if (fromUser) {
                if (rating < 0.5) {
                    moviesDetailViewModel.rateMovie(movie.id, 1f)
                    ratingBar.rating = 0.5f
                } else {
                    moviesDetailViewModel.rateMovie(movie.id, rating * 2)
                }
            }
        }

        moviesDetailViewModel.rateMovieLiveData.observe(this, Observer { rateMovie ->
            if (rateMovie.rated) {
                ratingBarMovie.rating = rateMovie.value / 2
                cancelRate.visibility = View.VISIBLE
            } else {
                ratingBarMovie.rating = 0f
                cancelRate.visibility = View.GONE
            }
        })
        if (savedInstanceState == null) {
            moviesDetailViewModel.getMovieDetails(movie.id)
            moviesDetailViewModel.getMovieAccountStates(movie.id)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return true
    }
}