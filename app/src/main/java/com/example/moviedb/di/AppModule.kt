package com.example.moviedb.di

import android.content.Context
import com.example.moviedb.AndroidApplication
import dagger.Binds
import dagger.Module


@Module
abstract class AppModule {
    @Binds
    abstract fun bindContext(application: AndroidApplication): Context

}