package com.example.moviedb.di

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.example.moviedb.BuildConfig
import com.example.moviedb.data.api.ApiProvider
import com.example.moviedb.broadcastreceiver.NetworkChangedBroadcastReceiver
import com.example.moviedb.data.api.RateMovieDeserializer
import com.example.moviedb.data.api.ApiKeyInterceptor
import com.example.moviedb.data.api.response.RateMovie
import com.example.moviedb.data.repository.movie.MovieRepository
import com.example.moviedb.data.repository.user.UserRepository
import com.example.moviedb.domain.service.MovieService
import com.example.moviedb.domain.service.UserService
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import io.reactivex.subjects.BehaviorSubject
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun providesRetrofit(): Retrofit {
        val clientBuilder = OkHttpClient().newBuilder()
        if (BuildConfig.DEBUG) {
            clientBuilder
                .addInterceptor(HttpLoggingInterceptor().apply {
                    level =
                        if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
                })
                .addInterceptor(ApiKeyInterceptor())
        }

        val gsonBuilder = GsonBuilder()
            .registerTypeAdapter(RateMovie::class.java, RateMovieDeserializer())
            .create()
        return Retrofit.Builder()
            .client(clientBuilder.build())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gsonBuilder))
            .baseUrl(BuildConfig.API_BASEURL)
            .build()
    }

    @Provides
    @Singleton
    fun providesSharedPreferences(context: Context): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(context)
    }

    @Provides
    fun providesMovieService(apiProvider: ApiProvider, internetStateSubject: BehaviorSubject<Boolean>): MovieService {
        return MovieRepository(apiProvider, internetStateSubject)
    }

    @Provides
    @Singleton
    fun providesInternetStateSubject(networkChangedBroadcastReceiver: NetworkChangedBroadcastReceiver): BehaviorSubject<Boolean> {
        return networkChangedBroadcastReceiver.internetStateSubject
    }

    @Provides
    @Singleton
    fun providesNetworkChangedBroadcastReceiver(): NetworkChangedBroadcastReceiver {
        return NetworkChangedBroadcastReceiver()
    }

    @Provides
    fun providesUserRepository(sharedPreferences: SharedPreferences, apiProvider: ApiProvider, internetStateSubject: BehaviorSubject<Boolean>): UserService {
        return UserRepository(sharedPreferences, apiProvider, internetStateSubject)
    }
}