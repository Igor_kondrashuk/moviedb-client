package com.example.moviedb.di

import com.example.moviedb.AndroidApplication
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AppModule::class,
        NetworkModule::class,
        ActivityBindingModule::class,
        AndroidSupportInjectionModule::class]
)
interface AppComponent : AndroidInjector<AndroidApplication> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<AndroidApplication>()
}