package com.example.moviedb.di

import com.example.moviedb.di.scope.ActivityScoped
import com.example.moviedb.ui.MainActivity
import com.example.moviedb.ui.login.LoginActivity
import com.example.moviedb.ui.login.LoginModule
import com.example.moviedb.ui.MainModule
import com.example.moviedb.ui.movies.detail.MovieDetailActivity
import com.example.moviedb.ui.movies.detail.MoviesDetailModule
import com.example.moviedb.ui.movies.detail.MoviesDetailViewModel
import com.example.moviedb.ui.movies.list.MoviesModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = [LoginModule::class])
    abstract fun loginActivity(): LoginActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [MainModule::class, MoviesModule::class])
    abstract fun mainActivity(): MainActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [MoviesDetailModule::class])
    abstract fun movieDetailActivity(): MovieDetailActivity
}