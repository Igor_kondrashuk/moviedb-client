package com.example.moviedb.broadcastreceiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import io.reactivex.subjects.BehaviorSubject

class NetworkChangedBroadcastReceiver: BroadcastReceiver() {

    val internetStateSubject = BehaviorSubject.create<Boolean>()

    override fun onReceive(context: Context?, intent: Intent?) {
        val connectivityManager =
            context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
        internetStateSubject.onNext(networkInfo?.isConnected ?: false)
    }

}