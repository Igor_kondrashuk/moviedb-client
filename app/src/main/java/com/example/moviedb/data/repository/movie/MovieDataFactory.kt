package com.example.moviedb.data.repository.movie

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.example.moviedb.data.api.ApiProvider
import com.example.moviedb.domain.entity.Movie
import io.reactivex.subjects.BehaviorSubject

class MovieDataFactory constructor(
    private val apiProvider: ApiProvider,
    private val internetStateSubject: BehaviorSubject<Boolean>,
    private val searchQuery: String,
    private val queryType: MovieDataSource.QueryType
) :
    DataSource.Factory<Long, Movie>() {
    val movieDataSourceLiveData = MutableLiveData<MovieDataSource>()

    override fun create(): DataSource<Long, Movie> {
        val movieDataSource =
            MovieDataSource(apiProvider, internetStateSubject, searchQuery, queryType)
        movieDataSourceLiveData.postValue(movieDataSource)
        return movieDataSource
    }
}