package com.example.moviedb.data.api

import com.example.moviedb.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response

class ApiKeyInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        return chain.proceed(
            chain.request().newBuilder().url(
                chain.request().url().newBuilder()
                    .addQueryParameter("api_key", BuildConfig.API_KEY)
                    .build()
            ).build()
        )
    }
}