package com.example.moviedb.data.api.response

import com.google.gson.annotations.SerializedName

class ErrorResponse(
    @SerializedName("status_code")
    val statusCode: StatusCode,
    @SerializedName("status_message")
    val statusMessage: String
)

enum class StatusCode {
    @SerializedName("1")
    SUCCESS,
    @SerializedName("2")
    INVALID_SERVICE,
    @SerializedName("3")
    AUTHENTICATION_FAILED_NOT_PERMISSION,
    @SerializedName("4")
    INVALID_FORMAT,
    @SerializedName("5")
    INVALID_PARAMETERS,
    @SerializedName("6")
    INVALID_ID,
    @SerializedName("7")
    INVALID_API_KEY,
    @SerializedName("8")
    DUPLICATE_ENTRY,
    @SerializedName("9")
    SERVICE_IS_OFFLINE,
    @SerializedName("10")
    SUSPENDED_API_KEY,
    @SerializedName("11")
    SOMETHING_WENT_WRONG,
    @SerializedName("12")
    ITEM_UPDATED_SUCCESSFULLY,
    @SerializedName("13")
    ITEM_DELETED_SUCCESSFULLY,
    @SerializedName("14")
    AUTHENTICATION_FAILED,
    @SerializedName("15")
    FAILED,
    @SerializedName("16")
    DEVICE_DENIED,
    @SerializedName("17")
    SESSION_DENIED,
    @SerializedName("18")
    VALIDATION_FAILED,
    @SerializedName("19")
    INVALID_ACCEPT_HEADER,
    @SerializedName("20")
    INVALID_DATE_RANGE,
    @SerializedName("21")
    ENTRY_NOT_FOUND,
    @SerializedName("22")
    INVALID_PAGE,
    @SerializedName("23")
    INVALID_DATE_FORMAT,
    @SerializedName("24")
    TIMEOUT_REQUEST,
    @SerializedName("25")
    REQUEST_COUNT_IS_OVER_LIMIT,
    @SerializedName("26")
    USERNAME_AND_PASSWORD_NOT_PROVIDED,
    @SerializedName("27")
    TOO_MANY_APPEND_TO_RESPONSE_OBJECTS,
    @SerializedName("28")
    INVALID_TIMEZONE,
    @SerializedName("29")
    ACTION_MUST_BE_CONFIRMED,
    @SerializedName("30")
    INVALID_USERNAME_AND_PASSWORD,
    @SerializedName("31")
    ACCOUNT_IS_DISABLED,
    @SerializedName("32")
    EMAIL_NOT_VERIFIED,
    @SerializedName("33")
    INVALID_REQUEST_TOKEN,
    @SerializedName("34")
    RESOURCE_NOT_FOUND


}