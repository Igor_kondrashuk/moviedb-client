package com.example.moviedb.data.api

import com.example.moviedb.data.api.response.MoviesResponse
import com.example.moviedb.domain.entity.Movie
import com.example.moviedb.domain.entity.MovieDetails
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieApi {

    @GET("movie/{movie_id}")
    fun getMovieDetails(@Path("movie_id") movieId: String): Single<MovieDetails>

    @GET("movie/popular")
    fun getPopularMovies(@Query("page") page: Int): Single<MoviesResponse>

    @GET("movie/top_rated")
    fun getTopRatedMovies(@Query("page") page: Int): Single<MoviesResponse>

    @GET("movie/upcoming")
    fun getUpcomingMovies(@Query("page") page: Int): Single<MoviesResponse>

    @GET("search/movie")
    fun getSearchMovies(@Query("query") query: String, @Query("page") page: Int): Single<MoviesResponse>
}