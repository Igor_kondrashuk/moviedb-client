package com.example.moviedb.data.api.response

import com.google.gson.annotations.SerializedName

data class MovieAccountStatesResponse(
    @SerializedName("id")
    val id: String,
    @SerializedName("favorite")
    val favorite: Boolean,
    @SerializedName("watchlist")
    val watchlist: Boolean,
    @SerializedName("rated")
    val rated: RateMovie
)

data class RateMovie(
    val rated: Boolean,
    val value: Float
)
