package com.example.moviedb.data.repository.movie

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.example.moviedb.domain.entity.Movie
import com.example.moviedb.ui.movies.list.LoadingState

data class MovieResult(
    val data: LiveData<PagedList<Movie>>,
    val loadingState: LiveData<LoadingState>
)