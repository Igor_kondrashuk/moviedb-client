package com.example.moviedb.data.repository.movie

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.example.moviedb.data.api.ApiProvider
import com.example.moviedb.domain.entity.Movie
import com.example.moviedb.ui.movies.list.LoadingState
import com.example.moviedb.util.callWhenInternetIsAvailable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject

class MovieDataSource(
    private val apiProvider: ApiProvider,
    private val internetStateSubject: BehaviorSubject<Boolean>,
    private val searchQuery: String,
    private val queryType: QueryType
) : PageKeyedDataSource<Long, Movie>() {

    val networkState = MutableLiveData<LoadingState>()

    private val disposables = CompositeDisposable()

    override fun loadInitial(params: LoadInitialParams<Long>, callback: LoadInitialCallback<Long, Movie>) {

        networkState.postValue(LoadingState.initLoading())

        callWhenInternetIsAvailable(
            internetStateSubject, Single.just(searchQuery)
                .flatMap {
                    if (it.isNotEmpty()) {
                        apiProvider.movieApi.getSearchMovies(searchQuery, 1)
                    } else {
                        when (queryType) {
                            QueryType.POPULAR -> apiProvider.movieApi.getPopularMovies(1)
                            QueryType.TOP_RATED -> apiProvider.movieApi.getTopRatedMovies(1)
                            QueryType.UPCOMING -> apiProvider.movieApi.getUpcomingMovies(1)
                        }
                    }

                })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { moviesResponse ->
                    callback.onResult(
                        moviesResponse.results,
                        null,
                        if (moviesResponse.page < moviesResponse.totalPages) moviesResponse.page + 1 else null
                    )
                    if (moviesResponse.results.isNotEmpty()) {
                        networkState.postValue(LoadingState.success())
                    } else {
                        networkState.postValue(LoadingState.empty())
                    }
                },
                { error ->
                    networkState.postValue(LoadingState.error(error))
                }
            ).addTo(disposables)
    }

    override fun loadAfter(params: LoadParams<Long>, callback: LoadCallback<Long, Movie>) {

        networkState.postValue(LoadingState.loading())

        callWhenInternetIsAvailable(
            internetStateSubject, Single.just(searchQuery)
                .flatMap {
                    if (it.isNotEmpty()) {
                        apiProvider.movieApi.getSearchMovies(searchQuery, params.key.toInt())
                    } else {
                        when (queryType) {
                            QueryType.POPULAR -> apiProvider.movieApi.getPopularMovies(params.key.toInt())
                            QueryType.TOP_RATED -> apiProvider.movieApi.getTopRatedMovies(params.key.toInt())
                            QueryType.UPCOMING -> apiProvider.movieApi.getUpcomingMovies(params.key.toInt())
                        }
                    }
                })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { moviesResponse ->
                    callback.onResult(
                        moviesResponse.results,
                        if (moviesResponse.page < moviesResponse.totalPages) moviesResponse.page + 1 else null
                    )
                    networkState.postValue(LoadingState.success())
                },
                { error ->
                    networkState.postValue(LoadingState.error(error))
                }
            ).addTo(disposables)
    }

    override fun loadBefore(params: LoadParams<Long>, callback: LoadCallback<Long, Movie>) {
    }


    override fun invalidate() {
        super.invalidate()
        disposables.clear()
    }

    enum class QueryType {
        POPULAR, TOP_RATED, UPCOMING
    }
}