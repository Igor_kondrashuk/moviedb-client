package com.example.moviedb.data.api

import com.example.moviedb.data.api.request.CreateSessionRequest
import com.example.moviedb.data.api.request.RateMovieRequest
import com.example.moviedb.data.api.request.ValidateWithLoginRequest
import com.example.moviedb.data.api.response.CreateSessionResponse
import com.example.moviedb.data.api.response.GetRequestTokenResponse
import com.example.moviedb.data.api.response.MovieAccountStatesResponse
import com.example.moviedb.data.api.response.ValidateWithLoginResponse
import com.example.moviedb.domain.entity.AccountDetails
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.*

interface LoginApi {

    @GET("authentication/token/new")
    fun getRequestToken(): Single<GetRequestTokenResponse>

    @POST("authentication/token/validate_with_login")
    fun validateWithLogin(@Body validateWithLoginRequest: ValidateWithLoginRequest): Single<ValidateWithLoginResponse>

    @POST("authentication/session/new")
    fun createSession(@Body createSessionRequest: CreateSessionRequest): Single<CreateSessionResponse>

    @GET("account")
    fun getAccountDetails(@Query("session_id") sessionId: String): Single<AccountDetails>

    @POST("movie/{movie_id}/rating")
    fun rateMovie(@Path("movie_id") movieId: String, @Query("session_id") sessionId: String, @Body rateMovieRequest: RateMovieRequest): Completable

    @DELETE("movie/{movie_id}/rating")
    fun deleteRatingMovie(@Path("movie_id") movieId: String, @Query("session_id") sessionId: String): Completable

    @GET("movie/{movie_id}/account_states")
    fun getMovieAccountStates(@Path("movie_id") movieId: String, @Query("session_id") sessionId: String): Single<MovieAccountStatesResponse>
}