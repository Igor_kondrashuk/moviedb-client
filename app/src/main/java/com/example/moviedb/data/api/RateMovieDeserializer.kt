package com.example.moviedb.data.api

import com.example.moviedb.data.api.response.RateMovie
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type

class RateMovieDeserializer: JsonDeserializer<RateMovie> {
    override fun deserialize(
        json: JsonElement,
        typeOfT: Type,
        context: JsonDeserializationContext
    ): RateMovie {
        var rated: Boolean
        var rating = 0.0f
        try {
            rated = json.asBoolean
        } catch (exception: Exception) {
            rated = true
            rating = context.deserialize(json.asJsonObject.get("value"), Float::class.java)
        }
        return RateMovie(rated, rating)
    }

}