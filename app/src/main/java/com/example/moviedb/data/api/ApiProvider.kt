package com.example.moviedb.data.api

import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ApiProvider @Inject constructor(retrofit: Retrofit) {
    val movieApi: MovieApi = retrofit.create(MovieApi::class.java)
    val loginApi: LoginApi = retrofit.create(LoginApi::class.java)
}