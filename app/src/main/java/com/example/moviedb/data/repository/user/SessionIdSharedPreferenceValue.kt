package com.example.moviedb.data.repository.user

import android.content.SharedPreferences
import com.example.moviedb.util.BaseSharedPreferenceValue
import com.example.moviedb.util.getObject
import com.example.moviedb.util.saveObject
import io.reactivex.Completable
import io.reactivex.Maybe

class SessionIdSharedPreferenceValue(private val sharedPreferences: SharedPreferences): BaseSharedPreferenceValue<String>() {
    companion object {
        const val SESSION_ID = "sessionId"
    }
    override fun saveObject(src: String): Completable {
        return sharedPreferences.saveObject(SESSION_ID, src)
    }

    override fun getObject(): Maybe<String> {
        return sharedPreferences.getObject(SESSION_ID)
    }

}