package com.example.moviedb.data.api.request

import com.google.gson.annotations.SerializedName

data class CreateSessionRequest(
    @SerializedName("request_token")
    val requestToken: String
)
