package com.example.moviedb.data.repository.user

import android.content.SharedPreferences
import com.example.moviedb.util.AuthException
import com.example.moviedb.data.api.ApiProvider
import com.example.moviedb.data.api.request.CreateSessionRequest
import com.example.moviedb.data.api.request.RateMovieRequest
import com.example.moviedb.data.api.request.ValidateWithLoginRequest
import com.example.moviedb.data.api.response.MovieAccountStatesResponse
import com.example.moviedb.domain.entity.AccountDetails
import com.example.moviedb.domain.service.UserService
import com.example.moviedb.util.CachedValue
import com.example.moviedb.util.callWhenInternetIsAvailable
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Singleton

@Singleton
class UserRepository(
    sharedPreferences: SharedPreferences,
    private val apiProvider: ApiProvider,
    private val internetStateSubject: BehaviorSubject<Boolean>
) : UserService {

    private val moviesCountSharedPreferenceValue =
        SessionIdSharedPreferenceValue(sharedPreferences)

    private val cacheSessionId = CachedValue(moviesCountSharedPreferenceValue)

    override fun createUserSession(login: String, password: String): Single<AccountDetails> {
        return callWhenInternetIsAvailable(internetStateSubject, apiProvider.loginApi.getRequestToken())
            .flatMap { getRequestTokenResponse ->
                callWhenInternetIsAvailable(
                    internetStateSubject, apiProvider.loginApi.validateWithLogin(
                        ValidateWithLoginRequest(login, password, getRequestTokenResponse.requestToken)
                    )
                )
                    .flatMap { validateWithLoginResponse ->
                        callWhenInternetIsAvailable(
                            internetStateSubject, apiProvider.loginApi.createSession(
                                CreateSessionRequest(validateWithLoginResponse.requestToken)
                            )
                        )
                            .flatMap { createSessionResponse ->
                                moviesCountSharedPreferenceValue.saveObject(createSessionResponse.sessionId)
                                    .toSingleDefault(createSessionResponse)
                            }
                            .flatMap { createSessionResponse ->
                                callWhenInternetIsAvailable(
                                    internetStateSubject,
                                    apiProvider.loginApi.getAccountDetails(createSessionResponse.sessionId)
                                )
                            }
                    }
            }
    }

    override fun getAccountDetails(): Single<AccountDetails> {
        return callIfLoggedIn<AccountDetails> { sessionId -> apiProvider.loginApi.getAccountDetails(sessionId) }
    }

    override fun getMovieAccountStates(movieId: String): Single<MovieAccountStatesResponse> {
        return callWhenInternetIsAvailable(
            internetStateSubject,
            callIfLoggedIn<MovieAccountStatesResponse> { sessionId ->
                apiProvider.loginApi.getMovieAccountStates(
                    movieId,
                    sessionId
                )
            })
    }

    override fun rateMovie(movieId: String, value: Float): Single<Float> {
        return callWhenInternetIsAvailable(
            internetStateSubject,
            callIfLoggedIn { sessionId ->
                apiProvider.loginApi.rateMovie(
                    movieId,
                    sessionId,
                    RateMovieRequest(value)
                )
            }.toSingle {
                value
            })
    }

    private fun <T> callIfLoggedIn(actionIfLoggedIn: (sessionId: String) -> Single<T>): Single<T> {
        return cacheSessionId.getCachedValue()
            .switchIfEmpty(Single.error(AuthException()))
            .flatMap { sessionId ->
                actionIfLoggedIn(sessionId)
            }
    }

    private fun callIfLoggedIn(actionIfLoggedIn: (sessionId: String) -> Completable): Completable {
        return cacheSessionId.getCachedValue()
            .switchIfEmpty(Single.error(AuthException()))
            .flatMapCompletable { sessionId ->
                actionIfLoggedIn(sessionId)
            }
    }

    override fun cancelRatingMovie(movieId: String): Completable {
        return callWhenInternetIsAvailable(
            internetStateSubject,
            callIfLoggedIn { sessionId -> apiProvider.loginApi.deleteRatingMovie(movieId, sessionId) })
    }

}