package com.example.moviedb.data.api.request

import com.google.gson.annotations.SerializedName

data class ValidateWithLoginRequest(
    @SerializedName("username")
    val username: String,
    @SerializedName("password")
    val password: String,
    @SerializedName("request_token")
    val requestToken: String
)
