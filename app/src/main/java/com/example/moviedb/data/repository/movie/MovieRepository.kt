package com.example.moviedb.data.repository.movie

import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import android.content.SharedPreferences
import com.example.moviedb.data.api.ApiProvider
import com.example.moviedb.domain.entity.Movie
import com.example.moviedb.domain.entity.MovieDetails
import com.example.moviedb.domain.service.MovieService
import com.example.moviedb.util.CachedValue
import com.example.moviedb.util.callWhenInternetIsAvailable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class MovieRepository @Inject constructor(
    private val apiProvider: ApiProvider,
    private val internetStateSubject: BehaviorSubject<Boolean>
) :
    MovieService {

    override fun getMovieById(movieId: String): Single<MovieDetails> {
        return callWhenInternetIsAvailable(
            internetStateSubject,
            apiProvider.movieApi.getMovieDetails(movieId)
        )
    }

    private val pagedListConfig = PagedList.Config.Builder()
        .setEnablePlaceholders(false)
        .setInitialLoadSizeHint(10)
        .setPageSize(20)
        .build()

    override fun getMovies(searchQuery: String, queryType: MovieDataSource.QueryType): MovieResult {
        val movieDataFactory =
            MovieDataFactory(apiProvider, internetStateSubject, searchQuery, queryType)

        val networkStateLiveData =
            Transformations.switchMap(movieDataFactory.movieDataSourceLiveData) { movieDataSource ->
                movieDataSource.networkState
            }

        val data = LivePagedListBuilder(movieDataFactory, pagedListConfig)
            .build()

        return MovieResult(data, networkStateLiveData)
    }

    override fun getMovieDetails(movieId: String) {
    }
}