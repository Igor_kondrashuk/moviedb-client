package com.example.moviedb.data.api.request

import com.google.gson.annotations.SerializedName

data class RateMovieRequest(
    @SerializedName("value")
    val value: Float
)
